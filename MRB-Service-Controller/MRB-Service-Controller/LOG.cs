﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace MRB_Service_Controller
{
    public static class Log
    {
        public static void Write(string line)
        {
            using(StreamWriter writer = new StreamWriter("Log-MRB.txt", true))
            {
                writer.WriteLine(DateTime.Now + " --> " + line);
            }
        }

    }
}