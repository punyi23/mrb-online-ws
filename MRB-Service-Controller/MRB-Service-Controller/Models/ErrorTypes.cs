﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MRB_Service_Controller.Models
{
    public enum ErrorTypes
    {
        OK,
        UNKNOWN,
        NOT_FOUND,
        EMAIL_DUPLICATED,
        WRONG_PASSWORD,
        USERNAME_USED,
        CREATURE_PREVIOUSLY_OWNED,
        NOT_ENOUGHT_COINS,
        ALREADY_ON_QUEUE
    }


}