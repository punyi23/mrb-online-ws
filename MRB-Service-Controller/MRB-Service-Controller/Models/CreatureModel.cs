﻿using MRB_Service_Controller.EntityControllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MRB_Service_Controller.Models
{
    public class CreatureModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Attack { get; set; }
        public int Defense { get; set; }
        public int Life { get; set; }
        public int Movement { get; set; }
        public string Photo { get; set; }
        public SpellModel Spell1 { get; set; }
        public SpellModel Spell2 { get; set; }
        public SpellModel Spell3 { get; set; }
        public SpellModel Spell4 { get; set; }
        public int Cost { get; set; }


        public CreatureModel()
        {

        }

        public CreatureModel(Creature creature)
        {
            Id = creature.Id;
            Name = creature.Name;
            Attack = creature.Attack;
            Defense = creature.Defense;
            Life = creature.Life;
            Movement = creature.Movement;
            Photo = creature.Photo;
            Spell1 = SpellBusiness.getSpellById((creature.Spell1 == null) ? -1 : (int)creature.Spell1);
            Spell2 = SpellBusiness.getSpellById((creature.Spell2 == null) ? -1 : (int)creature.Spell2);
            Spell3 = SpellBusiness.getSpellById((creature.Spell3 == null) ? -1 : (int)creature.Spell3);
            Spell4 = SpellBusiness.getSpellById((creature.Spell4 == null) ? -1 : (int)creature.Spell4);
            Cost = creature.Cost;
        }

        public Creature toDbCreature()
        {
            Creature creature = new Creature();
            creature.Id = Id;
            creature.Name = Name;
            creature.Attack = Attack;
            creature.Defense = Defense;
            creature.Life = Life;
            creature.Movement = Movement;
            creature.Photo = Photo;
            creature.Spell1 = Spell1.id;
            creature.Spell2 = Spell2.id;
            creature.Spell3 = Spell3.id;
            creature.Spell4 = Spell4.id;
            creature.Cost = Cost;
            return creature;
        }


        public override string ToString()
        {
            return Id + "-" + Name;
        }

        public SpellModel findSpell(int spellId)
        {
            if(Spell1.id == spellId)
            {
                return Spell1;
            }
            else if(Spell2.id == spellId)
            {
                return Spell2;
            }
            else if (Spell3.id == spellId)
            {
                return Spell3;
            }
            else if (Spell4.id == spellId)
            {
                return Spell4;
            }
            return null;
        }

    }
}