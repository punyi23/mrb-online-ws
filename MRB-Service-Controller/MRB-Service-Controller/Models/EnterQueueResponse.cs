﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MRB_Service_Controller.Models
{
    public class EnterQueueResponse
    {
        public UserModel user { get; set; }
        public ErrorTypes result { get; set; }
        public int QueuePosition { get; set; }
    }
}