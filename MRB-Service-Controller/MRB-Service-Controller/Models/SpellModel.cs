﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MRB_Service_Controller.Models
{
    public class SpellModel
    {
        public int id { get; set; }
        public string Name { get; set; }
        public int Range { get; set; }
        public int Power { get; set; }
        public bool Type { get; set; }
        public int Uses { get; set; }

        public SpellModel()
        {

        }

        public SpellModel(Spell spell)
        {
            id = spell.id;
            Name = spell.Name;
            Range = spell.Range;
            Power = spell.Power;
            Type = spell.Type;
            Uses = spell.Uses;
        }

        public Spell ToDbSpell()
        {
            Spell spell = new Spell();
            spell.id = id;
            spell.Name = Name;
            spell.Range = Range;
            spell.Power = Power;
            spell.Type = Type;
            spell.Uses = Uses;

            return spell;
        }
    }
}