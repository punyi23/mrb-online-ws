﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MRB_Service_Controller.Models
{
    public class BuyRequest
    {

        public UserModel user { get; set; }
        public CreatureModel creature { get; set; }

        public BuyRequest()
        {

        }

        public BuyRequest(UserModel user, CreatureModel creature)
        {
            this.user = user;
            this.creature = creature;
        }


    }
}