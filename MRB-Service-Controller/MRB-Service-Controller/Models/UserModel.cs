﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MRB_Service_Controller.Models
{
    public class UserModel
    {
        public int UserId { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string Nick { get; set; }
        public string Avatar { get; set; }
        public string LocationGPS { get; set; }
        public int Experience { get; set; }
        public int Money { get; set; }
        public string Email { get; set; }
        public bool isAdmin { get; set; }
        public bool isBanned { get; set; }
        public ErrorTypes error { get; set; }

        public UserModel()
        {

        }



        public UserModel(User usr)
        {
            UserId = usr.UserId;
            Username = usr.Username;
            Password = usr.Password;
            Nick = usr.Nick;
            Avatar = usr.Avatar;
            LocationGPS = usr.LocationGPS;
            Experience = usr.Experience;
            Money = usr.Money;
            Email = usr.Email;
            isAdmin = usr.isAdmin;
            isBanned = usr.isBanned;
        }

        public User toDbUser()
        {
            User user = new User();
            user.UserId = UserId;
            user.Username = Username;
            user.Password = Password;
            user.Nick = Nick;
            user.Avatar = Avatar;
            user.LocationGPS = LocationGPS;
            user.Experience = Experience;
            user.Money = Money;
            user.Email = Email;
            user.isAdmin = isAdmin;
            user.isBanned = isBanned;
            return user;
        }


        public override string ToString()
        {
            return Username + "-" + Password;
        }


    }
}