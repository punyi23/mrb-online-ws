﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MRB_Service_Controller.Models
{
    public class OwnedCreatureModel
    {

        public int UserId { get; set; }
        public int CreatureId { get; set; }
        public string GUID { get; set; }
        public DateTime buyDate { get; set; }
        public ErrorTypes error { get; set; }

        public OwnedCreatureModel()
        {

        }

        public OwnedCreatureModel(OwnedCreature creature)
        {
            UserId = creature.UserId;
            CreatureId = creature.CreatureId;
            GUID = creature.GUID;
            buyDate = creature.buyDate;
        }

        public OwnedCreature toDbOwnedCreature()
        {
            OwnedCreature creature = new OwnedCreature();
            creature.UserId = UserId;
            creature.CreatureId = CreatureId;
            creature.GUID = GUID;
            creature.buyDate = buyDate;
            return creature;
        }

    }
}