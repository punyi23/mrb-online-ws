﻿using MRB_Service_Controller.EntityControllers;
using MRB_Service_Controller.Game;
using MRB_Service_Controller.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

//todo se suman las colas


namespace MRB_Service_Controller
{
    public static class GameData
    {

        public static List<UserModel> USERS_QUEUE = new List<UserModel>();
        public static List<PairedCombat> PairedCombats = new List<PairedCombat>();
        static int MAX_COMBAT_CREATURES = 3;



        /// <summary>
        /// Encuentra un usuario en cola de espera
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public static UserModel findQueueUser(UserModel user)
        {
            foreach(UserModel u in USERS_QUEUE)
            {
                if(u.UserId == user.UserId)
                {
                    return u;
                }
            }
            return null;
        }

        /// <summary>
        /// Devuelve la posicion del usuario en cola de espera
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public static int findQueueUserPosition(UserModel user)
        {
            foreach (UserModel u in USERS_QUEUE)
            {
                if (u.UserId == user.UserId)
                {
                    return USERS_QUEUE.IndexOf(u);
                }
            }
            return -1;
        }

        /// <summary>
        /// Intenta emparejar dos usuario en la cola
        /// </summary>
        public static void tryPair()
        {
            foreach (UserModel user in USERS_QUEUE.FindAll(u => !u.isBanned))
            {
                foreach (UserModel u in USERS_QUEUE.FindAll(u => !u.isBanned))
                {
                    if (user != u && OwnedCreatureBusiness.GetOwnedCreatures(user).Count == OwnedCreatureBusiness.GetOwnedCreatures(u).Count)
                    {
                        PairedCombat combat = new PairedCombat(user, u);
                        PairedCombats.Add(combat);
                        USERS_QUEUE.Remove(user);
                        USERS_QUEUE.Remove(u);
                        return;
                    }
                }
            }
        }

        /// <summary>
        /// Se devuelve un PairedCombat en el caso que un usuario a sido emparejado
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public static PairedCombat findPairedUser(UserModel user)
        {
            
            foreach(PairedCombat combat in PairedCombats)
            {
                if(combat.user1.UserId == user.UserId || combat.user2.UserId == user.UserId)
                {
                    return combat;
                }
            }
            return null;
        }

        /// <summary>
        /// Inserta las criaturas iniciales de un jugador en un PairedCombat
        /// </summary>
        /// <param name="combat"></param>
        /// <returns></returns>
        public static PairedCombat setCombatUserCreatures(PairedCombat combat)
        {
            try
            {
                PairedCombat found = findPairedCombat(combat);
                int user1FoundCount = found.creatures.FindAll(c => c.ownedCreature.UserId == combat.user1.UserId).Count;
                int user2FoundCount = found.creatures.FindAll(c => c.ownedCreature.UserId == combat.user2.UserId).Count;
                int user1WantCount = combat.creatures.FindAll(c => c.ownedCreature.UserId == combat.user1.UserId).Count;
                int user2WantCount = combat.creatures.FindAll(c => c.ownedCreature.UserId == combat.user2.UserId).Count;

                if (user1FoundCount < user1WantCount && user1FoundCount + user1WantCount <= MAX_COMBAT_CREATURES)
                {
                    int cellCount = 1;
                    foreach(CombatCreature cre in combat.creatures)
                    {
                        cre.cell = cellCount + ":1";
                        cellCount++;
                    }
                    found.creatures.AddRange(combat.creatures);
                }
                if (user2FoundCount < user2WantCount && user2FoundCount + user2WantCount <= MAX_COMBAT_CREATURES)
                {
                    int cellCount = 1;
                    foreach (CombatCreature cre in combat.creatures)
                    {
                        cre.cell = cellCount + ":8";
                        cellCount++;
                    }
                    found.creatures.AddRange(combat.creatures);
                }
                return found;
            }
            catch (Exception e)
            {
            }
            return null;
        }


        /// <summary>
        /// Selector del tipo de acción
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public static PairedCombat processActionRequest(ActionRequest request)
        {
            switch (request.Type)
            {
                case ActionType.Attack:
                    return ProcessAttack(request);
                case ActionType.Move:
                    return ProcessMove(request);
                case ActionType.Turn:
                    return ProcessTurnSwitch(request);
                default:
                    return findPairedCombat(request.CombatGUID);
            }
        }


        /// <summary>
        /// Procesa Action cambio de turno
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        private static PairedCombat ProcessTurnSwitch(ActionRequest request)
        {
            PairedCombat combat = findPairedCombat(request.CombatGUID);
            if (request.UserId == combat.Turn.UserId && combat != null)
            {
                if(combat.Turn.UserId == combat.user1.UserId)
                {
                    combat.Turn = combat.user2;
                    return combat;
                }
                if (combat.Turn.UserId == combat.user2.UserId)
                {
                    combat.Turn = combat.user1;
                    return combat;
                }
            }
            return combat;
        }

        /// <summary>
        /// Procesa Action de movimiento
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        private static PairedCombat ProcessMove(ActionRequest request)
        {
            PairedCombat combat = findPairedCombat(request.CombatGUID);
            if (request.UserId == combat.Turn.UserId && combat != null)
            {
                CombatCreature sourceCreature = combat.creatures.Find(c => c.ownedCreature.GUID == request.SourceCreature);
                int currentRow = Convert.ToInt32(sourceCreature.cell.Split(':')[0]);
                int currentCol = Convert.ToInt32(sourceCreature.cell.Split(':')[1]);
                int wantedRow = Convert.ToInt32(request.TargetCell.Split(':')[0]);
                int wantedCol = Convert.ToInt32(request.TargetCell.Split(':')[1]);

                if(wantedCol > 0 && wantedCol < 9 && wantedRow > 0 && wantedRow < 4)
                {
                    if((wantedCol < currentCol && currentCol - wantedCol <= sourceCreature.creature.Movement) || 
                        (wantedCol > currentCol && wantedCol - currentCol <= sourceCreature.creature.Movement) ||
                        (wantedRow < currentRow && currentRow - wantedRow <= sourceCreature.creature.Movement) ||
                        (wantedRow > currentRow && wantedRow - currentRow <= sourceCreature.creature.Movement))
                    {
                        if((currentCol > 4 && wantedCol > 4) || (currentCol <= 4 && wantedCol <= 4))
                        {
                            sourceCreature.cell = wantedRow + ":" + wantedCol;

                            if (combat.Turn.UserId == combat.user1.UserId)
                            {
                                combat.Turn = combat.user2;
                                return combat;
                            }
                            if (combat.Turn.UserId == combat.user2.UserId)
                            {
                                combat.Turn = combat.user1;
                                return combat;
                            }
                        }
                    }
                }
            }
            return combat;
        }




        /// <summary>
        /// Procesa Action de Ataque
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        private static PairedCombat ProcessAttack(ActionRequest request)
        {
            PairedCombat combat = findPairedCombat(request.CombatGUID);
            if(request.UserId == combat.Turn.UserId && combat != null)
            {
                CombatCreature sourceCreature = combat.creatures.Find(c => c.ownedCreature.GUID == request.SourceCreature);
                CombatCreature targetCreature = combat.creatures.Find(c => c.ownedCreature.GUID == request.TargetCreature);
                SpellModel spell = sourceCreature.creature.findSpell(request.SpellId);
                if(sourceCreature.cell.Split(':')[0] != targetCreature.cell.Split(':')[0])
                {
                    return combat;
                }
                int sourceCol = Convert.ToInt32(sourceCreature.cell.Split(':')[1]);
                int targetCol = Convert.ToInt32(targetCreature.cell.Split(':')[1]);

                if (spell != null && (sourceCol < targetCol) ? (targetCol - sourceCol) <= spell.Range : (sourceCol - targetCol) <= spell.Range)
                {
                    targetCreature.creature.Life -= spell.Power - targetCreature.creature.Defense;

                    if(targetCreature.creature.Life <= 0)
                    {
                        combat.creatures.Remove(targetCreature);

                        if(combat.creatures.FindAll(c => c.ownedCreature.UserId == combat.user1.UserId).Count <= 0)
                        {
                            combat.user1 = null;
                            UserModel winnerUser = UserBusiness.GetUserData(combat.user2);
                            winnerUser.Money += 500;
                            UserBusiness.updateUser(winnerUser);
                            return combat;
                        }
                        if (combat.creatures.FindAll(c => c.ownedCreature.UserId == combat.user2.UserId).Count <= 0)
                        {
                            combat.user2 = null;
                            UserModel winnerUser = UserBusiness.GetUserData(combat.user1);
                            winnerUser.Money += 500;
                            UserBusiness.updateUser(winnerUser);
                            return combat;
                        }
                    }

                    if (combat.Turn.UserId == combat.user1.UserId)
                    {
                        combat.Turn = combat.user2;
                        return combat;
                    }
                    if (combat.Turn.UserId == combat.user2.UserId)
                    {
                        combat.Turn = combat.user1;
                        return combat;
                    }

                }
            }
            return combat;
        }


        /// <summary>
        /// Devuelve un combate completo y actualizado a través de un fragmento o un PairedCombat desactualizado
        /// </summary>
        /// <param name="combat"></param>
        /// <returns></returns>
        public static PairedCombat findPairedCombat(PairedCombat combat)
        {
            return PairedCombats.Find(c => c.CombatID == combat.CombatID); ;
        }

        /// <summary>
        /// Devuelve un Paired combat a través de un ID
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public static PairedCombat findPairedCombat(string CombatGUID)
        {
            return PairedCombats.Find(c => c.CombatID == CombatGUID); ;
        }

        /// <summary>
        /// Marca para eliminar un combate y lo deja en espera del usuario oponente o si ya la tiene lo destruye
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public static bool markCombatForDelete(markCombatForDeleteRequest request)
        {
            PairedCombat combat = PairedCombats.Find(p => request.CombatGUID == p.CombatID);
            if(combat.user1 != null && combat.user1.UserId == request.UserId)
            {
                if (request.Abandone)
                {
                    UserModel winnerUser = UserBusiness.GetUserData(combat.user1);
                    winnerUser.Money -= 100;
                    UserBusiness.updateUser(winnerUser);
                }
                combat.user1 = null;
            }
            if (combat.user2 != null && combat.user2.UserId == request.UserId)
            {
                if (request.Abandone)
                {
                    UserModel winnerUser = UserBusiness.GetUserData(combat.user2);
                    winnerUser.Money -= 100;
                    UserBusiness.updateUser(winnerUser);
                }
                combat.user2 = null;
            }


            if (combat.user2 == null && combat.user1 == null)
            {
                PairedCombats.Remove(combat);
            }
            return true;
        }

        
    }
}