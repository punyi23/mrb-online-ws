﻿using MRB_Service_Controller.EntityControllers;
using MRB_Service_Controller.Game;
using MRB_Service_Controller.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace MRB_Service_Controller.Controllers
{
    public class GameController : ApiController
    {



        /// <summary>
        /// Solicitud entrar en cola 
        /// </summary>
        /// <param name="queueRequest"></param>
        /// <returns></returns>
        [Route("mrb/enterQueue"), HttpPost]
        public HttpResponseMessage enterUserQueue([FromBody]EnterQueueResponse queueRequest)
        {
            UserModel userQueue;
            if((userQueue = GameData.findQueueUser(queueRequest.user)) == null)
            {
                GameData.USERS_QUEUE.Add(queueRequest.user);
                GameData.tryPair();
                return Request.CreateResponse(HttpStatusCode.OK, new EnterQueueResponse()
                {
                    user = queueRequest.user,
                    QueuePosition = GameData.findQueueUserPosition(queueRequest.user),
                    result = ErrorTypes.OK
                });
            }
            else
            {
                GameData.tryPair();
                return Request.CreateResponse(HttpStatusCode.OK, new EnterQueueResponse()
                {
                    user = queueRequest.user,
                    QueuePosition = GameData.findQueueUserPosition(queueRequest.user),
                    result = ErrorTypes.ALREADY_ON_QUEUE
                });
            }
        }

        /// <summary>
        /// Verificar si un usuario ya está emparejado
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        [Route("mrb/checkUserPaired"), HttpPost]
        public HttpResponseMessage checkUserPaired([FromBody]UserModel user)
        {
            return Request.CreateResponse(HttpStatusCode.OK, GameData.findPairedUser(user));
        }

        /// <summary>
        /// Inserta las criaturas por el usuario, una vez emparejado
        /// </summary>
        /// <param name="combat"></param>
        /// <returns></returns>
        [Route("mrb/setPlayerCombatCreatures"), HttpPost]
        public HttpResponseMessage setPlayerCombatCreatures([FromBody]PairedCombat combat)
        {
            return Request.CreateResponse(HttpStatusCode.OK, GameData.setCombatUserCreatures(combat));
        }

        /// <summary>
        /// Obtiene un PairedCombat en curso a través de su ID 
        /// </summary>
        /// <param name="combatGUID"></param>
        /// <returns></returns>
        [Route("mrb/getPairedCombat"), HttpPost]
        public HttpResponseMessage getPairedCombat([FromBody]string combatGUID)
        {
            return Request.CreateResponse(HttpStatusCode.OK, GameData.findPairedCombat(combatGUID));
        }

        /// <summary>
        /// Procesa una acción de combate
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [Route("mrb/processAction"), HttpPost]
        public HttpResponseMessage processAction([FromBody]ActionRequest request)
        {
            return Request.CreateResponse(HttpStatusCode.OK, GameData.processActionRequest(request));
        }


        /// <summary>
        /// Solicitud de marcar un combate para eliminar
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [Route("mrb/markForDelete"), HttpPost]
        public HttpResponseMessage markCombatForDelete([FromBody]markCombatForDeleteRequest request)
        {
            return Request.CreateResponse(HttpStatusCode.OK, GameData.markCombatForDelete(request));
        }

    }
}
