﻿using MRB_Service_Controller.EntityControllers;
using MRB_Service_Controller.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;



namespace MRB_Service_Controller.Controllers
{
    public class UsersController : ApiController
    {
        //todo: eliminar en modo produccion final!!!!
        //[Route("mrb/getAllUsers")]
        /*private HttpResponseMessage getAllUsers()
        {
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, UserBusiness.GetAllUsers());
            return response;
        }*/

        /// <summary>
        /// Solicitud de recibir un usuario completo a través de su ID
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        [Route("mrb/getUserData"), HttpPost]
        public HttpResponseMessage getUserData([FromBody]UserModel user)
        {
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, UserBusiness.GetUserData(user));
            return response;
        }


        /// <summary>
        /// Solicitud para iniciar sesion o registrarse
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        [Route("mrb/LogIn"), HttpPost]
        public HttpResponseMessage checkUserData([FromBody]UserModel user)
        {
            try
            {
                UserModel foundLoginUser = UserBusiness.checkUserData(user);
                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, foundLoginUser);
                return response;
            }
            catch (Exception e)
            {
                Log.Write(e.ToString());
            }
            return null;
        }


        /// <summary>
        /// Solicitud de actualizar información del usuario
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        [Route("mrb/updateUserData"), HttpPost]
        public HttpResponseMessage updateUserData([FromBody]UserModel user)
        {
            try
            {
                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, UserBusiness.updateUser(user));
                return response;
            }
            catch (Exception e)
            {
                Log.Write(e.ToString());
            }
            return null;
        }
    }
}