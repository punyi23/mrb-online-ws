﻿using MRB_Service_Controller.EntityControllers;
using MRB_Service_Controller.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;


namespace MRB_Service_Controller.Controllers
{
    public class OwnedCreatureController : ApiController
    {
        /// <summary>
        /// Solicitud de liberar criatura
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [Route("mrb/releaseCreature"), HttpPost]
        public HttpResponseMessage releaseCreature([FromBody]OwnedCreatureModel request)
        {
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, OwnedCreatureBusiness.releaseCreature(request));
            return response;
        }

        /// <summary>
        /// Solicitud para recibir la lista de OwnedCreatures
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        [Route("mrb/getOwnedCreatures"), HttpPost]
        public HttpResponseMessage getOwnedCreatures([FromBody]UserModel user)
        {
            try
            {
                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, OwnedCreatureBusiness.GetOwnedCreatures(user));
                return response;
            }
            catch (Exception e)
            {
                Log.Write(e.ToString());
            }
            return null;
        }
    }
}