﻿using MRB_Service_Controller.EntityControllers;
using MRB_Service_Controller.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace MRB_Service_Controller.Controllers
{
    public class ShopController : ApiController
    {
        /// <summary>
        /// Solicitud para recibir la lista de Creatures disponibles
        /// </summary>
        /// <returns></returns>
        [Route("mrb/returnCreatureList"), HttpGet]
        public HttpResponseMessage returnCreatureList()
        {
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, ShopBusiness.getAvailableCreatureList());
            return response;
        }

        /// <summary>
        /// Solicitud de compra de una Creature
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [Route("mrb/processBuy"), HttpPost]
        public HttpResponseMessage processBuy([FromBody]BuyRequest request)
        {
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, ShopBusiness.ProcessBuy(request));
            return response;
        }

        


    }
}
