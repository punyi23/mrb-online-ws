﻿using MRB_Service_Controller.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MRB_Service_Controller.EntityControllers
{
    public class ShopBusiness
    {
        
        /// <summary>
        /// Obtiene todas las criaturas disponibles para comprar
        /// </summary>
        /// <returns></returns>
        public static List<CreatureModel> getAvailableCreatureList()
        {
            List<Creature> query;
            List<CreatureModel> result = new List<CreatureModel>();
            using (GameDbEntities db = new GameDbEntities())
            {
                query = (from creatures in db.Creatures select creatures).ToList();
            }

            foreach(Creature cre in query)
            {
                result.Add(new CreatureModel(cre));
            }

            return result;
        }               

        /// <summary>
        /// Procesa una petición de compra pasandole un BuyRequest, y  devuelve un OwnedCreature
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public static OwnedCreatureModel ProcessBuy(BuyRequest request)
        {
            try
            {
                using (GameDbEntities db = new GameDbEntities())
                {
                    var result = from ownCre in db.OwnedCreatures
                                 where ownCre.UserId == request.user.UserId && ownCre.CreatureId == request.creature.Id
                                 select ownCre;
                    if(result.Count() > 0)
                    {
                        return new OwnedCreatureModel { error = ErrorTypes.CREATURE_PREVIOUSLY_OWNED };
                    }
                    else
                    {
                        if(request.user.Money >= request.creature.Cost)
                        {
                            OwnedCreature newCreature = new OwnedCreature()
                            {
                                CreatureId = request.creature.Id,
                                GUID = Guid.NewGuid().ToString(),
                                UserId = request.user.UserId,
                                buyDate = DateTime.Now
                            };
                            db.OwnedCreatures.Add(newCreature);
                            db.SaveChanges();
                            request.user.Money -= request.creature.Cost;
                            UserBusiness.updateUser(request.user);
                            return new OwnedCreatureModel(newCreature);
                        }
                        else
                        {
                            return new OwnedCreatureModel { error = ErrorTypes.NOT_ENOUGHT_COINS };
                        }
                    }
                }
            }
            catch (Exception e)
            {
                return new OwnedCreatureModel { error = ErrorTypes.UNKNOWN };
            }
        }

        
    }
}