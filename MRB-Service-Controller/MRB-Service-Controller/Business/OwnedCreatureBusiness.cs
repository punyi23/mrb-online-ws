﻿using MRB_Service_Controller.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MRB_Service_Controller.EntityControllers
{
    public class OwnedCreatureBusiness
    {
        /// <summary>
        /// Devuelve todas las criaturas que posee un usuario pasado como parámetro
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public static List<OwnedCreatureModel> GetOwnedCreatures(UserModel user)
        {
            List<OwnedCreatureModel> result = new List<OwnedCreatureModel>();
            try
            {
                using (GameDbEntities db = new GameDbEntities())
                {
                    var query = from ownCre in db.OwnedCreatures
                                where ownCre.UserId == user.UserId
                                select ownCre;

                    foreach (var item in query)
                    {
                        OwnedCreatureModel foundMonster = new OwnedCreatureModel(item);
                        result.Add(foundMonster);
                    }

                    //foreach (var item in query)
                    //{
                    //    var resultMonster = from u in db.Creatures
                    //                        where u.Id == item.CreatureId
                    //                        select u;
                    //    CreatureModel foundMonster = new CreatureModel(resultMonster.First());
                    //    result.Add(foundMonster);
                    //}
                }
            }
            catch (Exception e)
            {
            }
            return result;
        }

        /// <summary>
        /// Destruye una OwnedCreature de un usuario
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public static OwnedCreatureModel releaseCreature(OwnedCreatureModel request)
        {
            try
            {
                using (GameDbEntities db = new GameDbEntities())
                {
                    var query = from ownCre in db.OwnedCreatures
                                where ownCre.GUID == request.GUID
                                select ownCre;

                    if (query.Count() > 0)
                    {
                        OwnedCreature dbCreature = query.First();
                        db.OwnedCreatures.Remove(dbCreature);
                        db.SaveChanges();

                        return new OwnedCreatureModel { error = ErrorTypes.OK };
                    } else
                    {
                        return new OwnedCreatureModel { error = ErrorTypes.NOT_FOUND };

                    }
                }
            }
            catch (Exception e)
            {
                return new OwnedCreatureModel { error = ErrorTypes.UNKNOWN };
            }
        }
    }
}