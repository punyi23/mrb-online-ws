﻿using MRB_Service_Controller.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MRB_Service_Controller.EntityControllers
{
    public class SpellBusiness
    {
        /// <summary>
        /// Obtiene una spell a través de una ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static SpellModel getSpellById(int id)
        {
            try
            {
                if(id == -1)
                {
                    return null;
                }
                using (GameDbEntities db = new GameDbEntities())
                {
                    var query = from spell in db.Spells
                                where spell.id == id
                                select spell;

                    return new SpellModel(query.FirstOrDefault());
                }
            }
            catch (Exception e)
            {
            }
            return null;
        }

        
    }
}