﻿using MRB_Service_Controller.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MRB_Service_Controller.EntityControllers
{
    public class UserBusiness
    {
        //private static GameDbEntities dataContext = new GameDbEntities();


         
        
        /// <summary>
        /// recupera todos los usuarios de la db !!!!! todo: capturar excepciones
        /// </summary>
        /// <returns></returns>
        private static List<UserModel> GetAllUsers()
        {
            List<UserModel> query;
            using (GameDbEntities db = new GameDbEntities())
            {
                query = db.Users
                    .Select(a => new UserModel
                    {
                        Username = a.Username,
                        Password = a.Password,
                        Nick = a.Nick,
                        Avatar = a.Avatar,
                        LocationGPS = a.LocationGPS,
                        Experience = a.Experience,
                        Money = a.Money,
                        Email = a.Email,
                        isAdmin = a.isAdmin,
                        UserId = a.UserId,
                        isBanned = a.isBanned
                    }).ToList();
            }
            return query;
        }



        /// <summary>
        /// Obtiene todo un usuario pasando una ID
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public static UserModel GetUserData(UserModel user)
        {
            User queryUser;
            using (GameDbEntities db = new GameDbEntities())
            {
                queryUser = (from u in db.Users
                        where u.UserId == user.UserId
                        select u).FirstOrDefault();
                return new UserModel(queryUser);
            }
        }

        /// <summary>
        /// busca y devuelve un usuario pasado por parametro
        /// </summary>
        /// <param name="user"></param>
        /// <returns>UserModel o Null si no encuentra</returns>
        public static UserModel getUniqueUser(UserModel user)
        {
            try
            {
                using (GameDbEntities db = new GameDbEntities())
                {
                    var result = from u in db.Users
                                 where u.Username == user.Username && u.Password == user.Password
                                 select u;
                    UserModel foundUser = new UserModel(result.FirstOrDefault());
                    return foundUser;
                }
            }
            catch (Exception e)
            {
                return new UserModel { error = ErrorTypes.NOT_FOUND };
            }
           
        }


        /// <summary>
        /// Discrimina si debe logear o registrar y procesa la transaccion con la DB
        /// </summary>
        /// <param name="user"></param>
        /// <returns>UserModel si termina todo correctamente, null sino</returns>
        public static UserModel checkUserData(UserModel user)
        {
            if(user.Email == null || user.Email == "")
            {
                //intento de login
                return getUniqueUser(user);
            }
            else
            {
                //crar account
                UserModel foundUser = getUniqueUser(user);

                if (foundUser.Email == user.Email)
                {
                    return new UserModel { error = ErrorTypes.EMAIL_DUPLICATED };
                }
                else if (foundUser.Username == user.Username)
                {
                    return new UserModel { error = ErrorTypes.USERNAME_USED };
                }
                else if (foundUser.error == ErrorTypes.NOT_FOUND)
                {
                    using (GameDbEntities db = new GameDbEntities())
                    {
                        db.Users.Add(user.toDbUser());
                        db.SaveChanges();
                        return getUniqueUser(user);
                    }
                }
            }
            return new UserModel { error = ErrorTypes.UNKNOWN };
        }


        /// <summary>
        /// Actualiza un usuario verificado contra la base de datos
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public static UserModel updateUser(UserModel user)
        {
            using (GameDbEntities db = new GameDbEntities())
            {
                var query = from us in db.Users
                            where us.UserId == user.UserId
                            select us;
                User dbUser = query.FirstOrDefault();
                dbUser.Nick = user.Nick;
                dbUser.Avatar = user.Avatar;
                dbUser.Password = user.Password;
                dbUser.Email = user.Email;
                dbUser.isAdmin = user.isAdmin;
                dbUser.isBanned = user.isBanned;
                dbUser.Money = user.Money;
                dbUser.LocationGPS = user.LocationGPS;
                dbUser.Experience = user.Experience;


                db.SaveChanges();
                return getUniqueUser(user);
            }
        }

    }
}