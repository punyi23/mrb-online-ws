﻿using MRB_Service_Controller.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MRB_Service_Controller.Game
{
    public class PairedCombat
    {

        public String CombatID { get; set; }
        public UserModel user1 { get; set; }
        public UserModel user2 { get; set; }
        public List<CombatCreature> creatures { get; set; }
        public UserModel Turn { get; set; }

        public PairedCombat(UserModel user1, UserModel user2)
        {
            CombatID = Guid.NewGuid().ToString();
            Turn = user1;
            this.user1 = user1;
            this.user2 = user2;
            user1.Password = "";
            user2.Password = "";

            if(creatures == null)
            {
               creatures = new List<CombatCreature>();
            }
        }

        public void switchTurn()
        {
            if(Turn.UserId == user1.UserId)
            {
                Turn = user2;
            }
            else
            {
                Turn = user1;
            }
        }
    }
}