﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MRB_Service_Controller.Game
{
    public class ActionRequest
    {
        public ActionType Type { get; set; }
        public int UserId { get; set; }
        public string CombatGUID { get; set; }
        public string SourceCreature { get; set; }
        public string TargetCreature { get; set; }
        public int SpellId { get; set; }
        public string TargetCell { get; set; }
        
        public ActionRequest()
        {
            
        }     



    }

    public enum ActionType
    {
        Turn, 
        Attack, 
        Move
    }
}