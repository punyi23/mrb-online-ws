﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MRB_Service_Controller.Game
{
    public class markCombatForDeleteRequest
    {
        public string CombatGUID { get; set; }
        public int UserId { get; set; }
        public bool Abandone { get; set; }
    }
}