﻿using MRB_Service_Controller.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MRB_Service_Controller.Game
{
    public class CombatCreature
    {
        

        public CreatureModel creature { get; set; }
        public OwnedCreature ownedCreature { get; set; }
        public String cell { get; set; }

        public CombatCreature(CreatureModel creature, OwnedCreature ownedCreature)
        {
            this.creature = creature;
            this.ownedCreature = ownedCreature;
        }

        public CombatCreature(CreatureModel creature, OwnedCreature ownedCreature, String cell)
        {
            this.creature = creature;
            this.ownedCreature = ownedCreature;
            this.cell = cell;
        }

        public CombatCreature()
        {

        }

    }
}